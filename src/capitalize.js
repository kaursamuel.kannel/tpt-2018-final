module.exports = function capitalize(f) {
    if (typeof f !== 'string') {
      throw new Error('bad input');
    }
    return f[0].toUpperCase() + f.slice(1);
  };
