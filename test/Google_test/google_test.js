var config = require('../../nightwatch.conf.js');

module.exports = {
    'google test': function(browser) {
        browser
            .resizeWindow(1280, 800)
            .url('https://google.ee/')
            .waitForElementVisible('body div#main')
            .pause(2000)
            .setValue('input[class="gLFyf gsfi"]', ['tallinn', browser.Keys.ENTER])
            .pause(2000)
            .assert.containsText('body','tallinn')
            .saveScreenshot(config.imgpath(browser) + 'tallinsearch.png')
            .pause(5000)
            .click('h3[class="LC20lb"]')
            .saveScreenshot(config.imgpath(browser) + 'firsttallinresult.png')
            .pause(2000)
            .end();
    }
};
