const users = require('../../src/user');

test('Exception', () => {
    
    expect(() => {
        users("Kaur Kannel")
    }).toThrowError("id needs to be integer");
});
